import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 *
 * @author Humberto Miranda
 */
class ThreadJogadores extends Thread{
    Socket conexao;
    String nome="sementrada";
    Forca forca;
    int pontuacao = 0;
    ServidorJogoDaForca servidorJogoForca;
    BufferedReader doCliente;   
    int numJogador;
    int flagInicia = 0;
    String comando;
    int pontos = 0;

    DataOutputStream paraCliente;
    
    public ThreadJogadores( Socket conexao, ServidorJogoDaForca servidorJogoForca, Forca forca ) {
        try {
            this.conexao = conexao;
            this.servidorJogoForca = servidorJogoForca;
            this.forca = forca;
            numJogador = servidorJogoForca.getServidores().size()+1;
            doCliente = new BufferedReader(new InputStreamReader(conexao.getInputStream()));
            paraCliente = new DataOutputStream(conexao.getOutputStream());
        } catch (Exception e) {
           e.printStackTrace();
        }
     }
    
    public void ganhou(){
    	setPontos(getPontos()+1);
    }
    
    public void setPontos(int pontos){
    	this.pontos = pontos;
    }
    
    public int getPontos(){
    	return pontos;
    }
    
     public void run() {
      try {
         paraCliente.writeBytes("Qual seu nome? ");
         nome = doCliente.readLine();
         boolean inicia=true;
         for (ThreadJogadores servidor2: servidorJogoForca.getServidores()) {
        	 
	         if(servidor2.nome.equals("sementrada")){
	        	 inicia = false;
	         }
         }
         
         if(inicia){
        	 if(servidorJogoForca.getServidores().size() == 3){
        		 servidorJogoForca.iniciaGame(servidorJogoForca);
        	 }
         }
         
         while (true) {
            comando = doCliente.readLine();
            paraCliente.writeBytes("\r");
            if (comando==null) {
                System.out.println("Conexao com cliente perdida!");
                System.out.println("Conexao sera fechada.");
                conexao.close();
                servidorJogoForca.getServidores().remove(servidorJogoForca.getServidores().size()-1); //Elimina jogador quando perde a conexao socket.
                return;
            } else {
            	comando = forca.tratarMensagem(this); //chama o metodo tratar, que faz parte da logica do jogo e envia sua propria classe como parametro
            	servidorJogoForca.notificaJogadores(comando);
            }
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
}
