				EAP - Jogo da Forca

	1 - Gerenciamento do Projeto
	2 - Documentação
		2.1 Descrição das Classes
		2.2 Casos de Uso
		2.3 Diagrama de Estados
		2.4 Fluxo de Desenvolvimento
		2.5 Estrutura Analítica do Projeto
		2.6 Arquivo da Descrição de Regras
	3 - Desenvolvimento
		3.1 Classes
			3.1.1 Classe Servidor
			3.1.2 Classe Palavra
			3.1.3 Classe Forca
			3.1.4 Classe Threads
		3.2 Validação e Teste
