				EAP (Com Lista de Atividades)- Jogo da Forca

	1 - Gerenciamento do Projeto
	2 - Documenta��o
		2.1 Descri��o das Classes
			2.1.1 Descrever Classes
		2.2 Casos de Uso
			2.2.1 Criar Casos de Uso
		2.3 Diagrama de Estados
			2.3.1 Criar Diagrama de Estados
		2.4 Fluxo de Desenvolvimento
			2.4.1 Criar Fluxo de Desenvolvimento
		2.5 Estrutura Anal�tica do Projeto
			2.5.1 Criar Estrutura Anal�tica do Projeto
		2.6 Arquivo da Descri��o de Regras
			2.6.1 Escrever arquivo com as Regras
	3 - Desenvolvimento
		3.1 Classes
			3.1.1 Classe Servidor
				3.1.1.1 Implementar Servidor
			3.1.2 Classe Palavra
				3.1.2.1	Implementar Classe Palavra
			3.1.3 Classe Forca
				3.1.3.1 Implementar Classe Forca
			3.1.4 Classe Threads
				3.1.4.1 Implementar Threads
		3.2 Valida��o e Teste
			3.2.1 Testar Jogo
			
			
			*****AS FOLHAS DA EAP (ULTIMO NIVEL) CORRESPONDEM �S ATIVIDADES*****
