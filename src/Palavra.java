import java.util.Vector;

/**
 *
 * @author Humberto Miranda
 */
public class Palavra {

        public Palavra(String categoria, String palavra){
                setCategoria(categoria);
                setPalavra(palavra);
        }
        
        private String categoria;
        private String palavra;
        
        public void setCategoria(String categoria){
            this.categoria = categoria;
        }
        
        public String getCategoria(){
            return categoria;
        }
        
        public String getPalavra() {
                return palavra;
        }
        public void setPalavra(String palavra) {
                this.palavra = palavra;
        }
        

    	public String mascararPalavra (String palavra, Vector<Character> letras, Vector<Character> letrasUtilizadas) {
    		boolean letra = true; 
    		  
    		String resposta = "";
    		for(int i=0; i< palavra.length(); i++){
    			for (int j=0; j<letras.size(); j++) {
    				if(palavra.charAt(i) == letras.get(j)) {
    					resposta = resposta + palavra.charAt(i);
    					letra = false;
    				}
    			}
    			if (letra) {
    				resposta = resposta + "_ ";
    			}
    			letra = true;
    		}
    		
    		
    		resposta = resposta + "      eh um "+getCategoria()+"              |";
    		
    		for(int i=0; i< letrasUtilizadas.size(); i++){
    			resposta = resposta+letrasUtilizadas.get(i);
    		}
    		return resposta;
    	}
    	
    	public String mascararPalavra (String palavra) {
    		String resposta = "";
//    		for(int i=0; i< palavra.length(); i++){
//    			resposta = resposta + ".";
//    		}
    		resposta = "A palavra contem "+palavra.length()+" letras.";
    		return resposta;
    	}
}
