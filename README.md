## Projeto 2 Grupo 4 - Jogo da Forca 


### Universidade Federal de Goiás - ADSI2015/1 - Professor Akira


##### Descrição

* Jogo multiusuário onde o servidor irá fornecer palavras classificadas por categoria para que os usuários possam "adivinhar";

* Cada usuário receberá uma quantidade X de pontos por cada letra acertada e Y por cada palavra acertada;

* O jogo poderá conter até 5 jogadores;

* O jogo terá até 5 rodadas dependendo da quantidade de jogadores para que cada um posso inicar o jogo em uma rodada;