import java.io.IOException;
import java.util.Vector;

/**
 *
 * @author Diogo Leal
 */

public class Forca {
	int vez;
	ServidorJogoDaForca servidor;
	Palavra palavra;
	Vector<Character> letrasUtilizadas = new Vector<Character>();
	Vector<Character> letrasContidas = new Vector<Character>();
	boolean letraRepetida = false;
	
	public int getVez() {
		return vez;
	}

	public void setVez(int vez) {
		this.vez = vez;
	}
	
	public Forca () {
		setVez(0);
	}
	
	public void mostrarPalavra(){
		
	}
	
	public String tratarMensagem (ThreadJogadores jogador) throws IOException {
		palavra = jogador.servidorJogoForca.getPalavre();
		String resposta = "";
		char letraJogador;
		
		if (verificaVez(jogador.numJogador)) {
			if(jogador.comando.length()!=1){
				trocarVez(getVez());
				//Verifica se a entrada do jogador � igual a palavra em jogo
				if(jogador.comando.equals(palavra.getPalavra())){
					jogador.servidorJogoForca.notificaJogadores("O vencedor foi o jogador "+jogador.nome.toUpperCase()+"! PARABENS!!!\n"); //acho que podemos fazer a leitura do nome do jogador e colocar aqui
					jogador.servidorJogoForca.notificaJogadores("A palavra era: "+jogador.servidorJogoForca.palavra.getPalavra()+"\r\n"); //acho que podemos fazer a leitura do nome do jogador e colocar aqui
					
					jogador.servidorJogoForca.setStatus(true);
					jogador.servidorJogoForca.palavra = null; //seta palavra como null
					jogador.ganhou();
					
					jogador.servidorJogoForca.iniciaGame(jogador.servidorJogoForca);
					letrasUtilizadas.clear(); //limpa o vector para a nova palavra
					letrasContidas.clear(); //limpa o vector para a nova palavra
					//trocarVez(getVez()); //troca a vez //NAO PRECISA TROCAR A VEZ NESSE MOMENTO
					jogador.servidorJogoForca.notificaJogadores(jogador.servidorJogoForca.palavra.mascararPalavra(jogador.servidorJogoForca.palavra.getPalavra(), letrasContidas, letrasUtilizadas));
				}else{
					resposta = "Errou a palavra\n\r" + palavra.mascararPalavra(palavra.getPalavra(), letrasContidas, letrasUtilizadas);
				}
				
				letraRepetida=false;
			}else{
				//verifica se o caracter de entrada do jogador esta contido na palavra em jogo
				letraJogador = jogador.comando.charAt(0); 
				for(int i=0; i< letrasUtilizadas.size(); i++){
					if(letraJogador==letrasUtilizadas.get(i)){
						resposta = "Essa letra ja foi utilizada. Tente Novamente\n\r" + palavra.mascararPalavra(palavra.getPalavra(), letrasContidas, letrasUtilizadas);
						letraRepetida=true;
					}
				}
				
				if (letraRepetida) {

				} else {
					letrasUtilizadas.add(letraJogador);
					if(palavra.getPalavra().contains(Character.toString(letraJogador))){
						letrasContidas.add(letraJogador);
						jogador.paraCliente.writeBytes("------------------------------------------------------\r\nVoce acertou, jogue de novo por favor.\r\n");
						resposta = palavra.mascararPalavra(palavra.getPalavra(), letrasContidas,letrasUtilizadas);
					} else {
						resposta = "O jogador " + jogador.nome + " errou a letra e perdeu a vez.\n\r" + palavra.mascararPalavra(palavra.getPalavra(), letrasContidas, letrasUtilizadas);
						trocarVez(getVez());
					}
				}
				letraRepetida=false;
			}
			
		} else if  (vez == 0) {
			resposta = "Calma manolo! O jogo nao comecou ainda!"; 
		} else {
			resposta = "Calma manolo! Nao eh sua vez";
		}
		
		
		int i = 0;
		for (ThreadJogadores servidor: jogador.servidorJogoForca.getServidores()) {
			if(i==getVez()-1){
				//jogador.servidorJogoForca.notificaJogadores(servidor.nome.toUpperCase() + " sua vez!");
				resposta = resposta + "\n\r" + servidor.nome.toUpperCase() + " sua vez!";
			}
			i++;
	    }
		
		return resposta;
	}
	
	public boolean verificaVez(int numJogador) {
		if (numJogador == vez) {
			return true;
		} else {
			return false;
		}
	}
	
	public void trocarVez (int vezatual) {
		if (vezatual != 3) {
			vezatual++;
			setVez(vezatual);
		} else {
			setVez(1);
		}
	}
}