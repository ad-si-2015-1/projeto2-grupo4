import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Vector;

/**
 *
 * @author Humberto Miranda
 */
public class ServidorJogoDaForca {
    
	Vector<ThreadJogadores> servidores =  new Vector<ThreadJogadores>();
    static int porta = 6789;
    public Vector<Palavra> palavras;
    ServerSocket socketServidor;
    Palavra palavra;
    Vector<Integer> numJogadores;
    int i=0;
    static boolean statusJogo = true;
    String mostrarJogadores;
    static Forca forca;
    
    public Palavra getPalavre(){
    	return palavra;
    }
    
    public boolean getStatus(){
    	return statusJogo;
    }
    
    public void setStatus(boolean status){
    	this.statusJogo = status;
    }
    
    public int getI(){
        i++;
        return i;
    }
    
    
//    public ServidorJogoDaForca(){}
    
    /**
     * Inicializacao do Servidor escutando na porta 6789
     * @param porta 
     */
    public ServidorJogoDaForca(int porta){
        try{
            this.socketServidor = new ServerSocket(porta);
            System.out.println("Servidor iniciado na porta " + porta);
        }catch(Exception e){
            System.out.println("Erro na inicializacao do Servidor!");
        }
    }
    
    /**
     * Inicializacao do vetor de palavras que poderao ser utilizadas.
     * O primeiro atributo do vetor com a categoria da palavra
     * O segundo e a palavra
     */
    public void inicializaPalavras(){
         palavras = new Vector<Palavra>();
         palavras.add(new Palavra("animal","cachorro"));
         palavras.add(new Palavra("veiculo","carro"));
         palavras.add(new Palavra("fruta","morango"));
         palavras.add(new Palavra("objeto","cadeira"));
         palavras.add(new Palavra("objeto","mesa"));
         palavras.add(new Palavra("animal","gato"));
         palavras.add(new Palavra("animal","hipopotomo"));
         palavras.add(new Palavra("animal","elefante"));
         palavras.add(new Palavra("fruta","melancia"));
         palavras.add(new Palavra("veiculo","trem"));
         palavras.add(new Palavra("fruta","carambola"));
         palavras.add(new Palavra("objeto","teclado"));
         palavras.add(new Palavra("objeto","computador"));
         palavras.add(new Palavra("animal","papagaio"));
         palavras.add(new Palavra("animal","leopardo"));
         palavras.add(new Palavra("animal","leao"));
         palavras.add(new Palavra("fruta","pitanga"));
         palavras.add(new Palavra("veiculo","metro"));
         palavras.add(new Palavra("animal","galinha"));
         palavras.add(new Palavra("veiculo","bicicleta"));
         palavras.add(new Palavra("fruta","jobuticaba"));
         palavras.add(new Palavra("objeto","caneta"));
         palavras.add(new Palavra("objeto","armario"));
         palavras.add(new Palavra("animal","lagarto"));
         palavras.add(new Palavra("animal","peixe"));
         palavras.add(new Palavra("animal","orangotango"));
         palavras.add(new Palavra("fruta","laranja"));
         palavras.add(new Palavra("objeto","impressora"));
         palavras.add(new Palavra("objeto","espelho"));
         palavras.add(new Palavra("animal","pato"));
         palavras.add(new Palavra("animal","galo"));
         palavras.add(new Palavra("animal","tatu"));
         palavras.add(new Palavra("fruta","caja"));
         palavras.add(new Palavra("fruta","caju"));
         palavras.add(new Palavra("fruta","seriguela"));
         palavras.add(new Palavra("fruta","kiwi"));
         palavras.add(new Palavra("animal","girafa"));
         palavras.add(new Palavra("animal","rinoceronte"));
         palavras.add(new Palavra("animal","camelo"));
         palavras.add(new Palavra("animal","carneiro"));
    }
    
    Palavra getPalavra(int index){
    	return palavras.get(index); 
    }
    
    Vector<Palavra> getArrayPalavras(){
    	return palavras;
    }
    
   ServerSocket getConexaoServidor() {
      return socketServidor;
   }

   Vector<ThreadJogadores> getServidores() {
      return servidores;
   }
   
   void notificaJogadores(String mensagem) throws IOException {
	      for (ThreadJogadores servidor: getServidores()) {
	         servidor.paraCliente.writeBytes(mensagem + "\n\r");
	      }      
	   }
   
   public void iniciaGame(ServidorJogoDaForca servidor) throws IOException{
			//l�gica de inicio de game com inicializa��o da palavra
		    servidor.notificaJogadores("-----------------------------------------------------------\r"); //exibe os pontinhos de quantos palavras tem
			servidor.notificaJogadores("\r\n"); //exibe os pontinhos de quantos palavras tem
			servidor.notificaJogadores("Pontos "+ getServidores().get(0).nome +": " + getServidores().get(0).pontos +"\r\n"); //exibe os pontinhos de quantos palavras tem
			servidor.notificaJogadores("Pontos "+ getServidores().get(1).nome +": " + getServidores().get(1).pontos +"\r\n"); //exibe os pontinhos de quantos palavras tem
			servidor.notificaJogadores("Pontos "+ getServidores().get(2).nome +": " + getServidores().get(2).pontos +"\r\n"); //exibe os pontinhos de quantos palavras tem
			servidor.notificaJogadores("\r\n"); //exibe os pontinhos de quantos palavras tem
			servidor.notificaJogadores("-----------------------------------------------------------\r\n"); //exibe os pontinhos de quantos palavras tem
			Random rand = new Random();
			int numIndexPalavra = rand.nextInt((getArrayPalavras().size()));
			palavra = getPalavra(numIndexPalavra);
			//setStatus(true);
			//System.out.println(palavra.getPalavra()); //imprime a palavra 
			servidor.notificaJogadores("A categoria da Palavra e " + palavra.getCategoria()+"\r"); //exibe a categoria para o usu�rio
			servidor.notificaJogadores(palavra.mascararPalavra(palavra.getPalavra())+"\r"); //exibe os pontinhos de quantos palavras tem
			if(servidor.getStatus()){
				if(forca.getVez()==0){
					//servidor.notificaJogadores("Inicie o Jogo " +servidor.getServidores().get(0).nome); //exibe os pontinhos de quantos palavras tem
				}else{
					//servidor.notificaJogadores("Inicie o Jogo " +servidor.getServidores().get(forca.getVez()-).nome); //exibe os pontinhos de quantos palavras tem
				}
				servidor.notificaJogadores("Inicie o Jogo " +servidor.getServidores().get(forca.getVez()-1).nome.toUpperCase()); //exibe os pontinhos de quantos palavras tem
				// System.out.println(forca.getVez()); //exibe a vez do jogador que est� iniciando o jogo
				servidor.setStatus(false);
			}
			
	   }
	
    public static void main(String args[]) throws IOException{
        ServidorJogoDaForca servidorJogoForca = new ServidorJogoDaForca(porta);
        forca = new Forca();
        forca.setVez(1);
        servidorJogoForca.inicializaPalavras();

        while (true) {
            Socket conexao = servidorJogoForca.getConexaoServidor().accept(); 
            if(servidorJogoForca.getServidores().size()<3){
                //Instancia de ThreadJogadore para cada Jogador que entrar, no maximo 3
                ThreadJogadores st = new ThreadJogadores( conexao, servidorJogoForca , forca );
                servidorJogoForca.getServidores().add(st);
                st.start();
                if (servidorJogoForca.getServidores().size() == 3 && servidorJogoForca.getStatus()) {
                	forca.setVez(1);//passa a vez para o primeiro jogador dar inicio ao jogo
                	 //servidorJogoForca.iniciaGame(servidorJogoForca);
                }
            } else {
            	DataOutputStream paraCliente;
            	paraCliente = new DataOutputStream(conexao.getOutputStream());
                paraCliente.writeBytes("Quantidade maxima de jogadores atingida. Voc� sera desconectado.");
                System.out.println("Quantidade Maxima de Usuarios Preenchida!");
                conexao.close();
            }
        }
    }
}
