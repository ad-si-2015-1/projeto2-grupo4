﻿### FLUXO DE DESENVOLVIMENTO DO GRUPO 4 NO AMBIENTE GIT - PROJETO 1
----

#### Branch Master


Na branch master conterá alguns arquivos entre eles arquivos(COLABORADORES.md e README.md) e 2 diretórios(docs e src_code);

* Arquivo COLABORADORES.md -> Deve conter o nome e email de todos os participantes do grupo;
* Arquivo README.md -> Descrição breve do Projeto 2
* Diretório docs -> Terá toda a documentação do projeto(artefatos de documentação, arquivos de imagem, arquivos pdf);
* Diretório src_code -> Terá apenas arquivos JAVA(arquivo.java);


#### Branch Projeto2

* Todos os participantes do grupo irão fazer commit diretamente nessa branch, sendo inicialmente um clone da Branch Master;
* Na Branch Projeto2 o participante irá desenvolver todo os seus artefatos(documentação ou codificação) seguindo as descrições da Branch Master;
* Após a validação dos artefatos da Branch Projeto2 o colaborador deverá realizar o commit à Branch Master

*OBS: NENHUM ARQUIVO PODERÁ SER COMITADO NA RAIZ DA BRANCH APENAS NOS DIRETÓRIOS docs e src_code EXCETO ALTERAÇÃO DOS ARQUIVOS README.MD e COLABORADORES.MD SEGUINDO DESCRIÇÃO DA BRANCH MASTER;*


*TODA A CONTABILIZAÇÃO DOS MRs SERÁ FEITA COM BASE NA BRANCH MASTER;*

# ![fluxo_desenvolvimento](imagens/img_fluxo_desenvolvimento.jpg)